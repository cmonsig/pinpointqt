# -*- coding: utf-8 -*-
"""
/***************************************************************************
 pinpointQt
                                 A QGIS plugin
 This plugin drops pins on a basemap and displays the pins features when clicked on
                              -------------------
        begin                : 2019-02-17
        git sha              : $Format:%H$
        copyright            : (C) 2019 by Simone Glinz
        email                : simone@glinz.li
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from qgis.utils import iface
from qgis.core import *
from qgis.gui import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *
# Initialize Qt resources from file resources.py
import resources
# Import the code for the dialog
from pinpoint_Qt_dialog import pinpointQtDialog
import os.path

from .util import *
import olc



class pinpointQt:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgisInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'pinpointQt_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = pinpointQtDialog()

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&pinpointQt')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'pinpointQt')
        self.toolbar.setObjectName(u'pinpointQt')

        # original tutorial code
        # self.dlg.lineEdit.clear()
        # self.dlg.pushButton.clicked.connect(self.select_output_file)



    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('pinpointQt', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """


        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/pinpointQt/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Drop pins on a basemap'),
            callback=self.run,
            parent=self.iface.mainWindow())


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&pinpointQt'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar


    # Commenting this out, is not needed
    # def select_output_file(self):
    #     filename = QFileDialog.getSaveFileName(self.dlg, "Select output file ", "", '*.txt')
    #     self.dlg.lineEdit.setText(filename)


    def onFeatureIdentified(self, ff):
        print("feature selected")

    def PinPointlayerExists(self):
        layers = QgsMapLayerRegistry.instance().mapLayers().values()
        for i in layers:
            if i.name() == "Pins":
                return True
                # break
        return False


    def run(self):
        """Run method that performs all the real work"""

        layers = self.iface.legendInterface().layers()
        layer_list = []
        for layer in layers:
            layer_list.append(layer.name())

        self.dlg.comboBox.addItems(layer_list)

        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:

            filename = self.dlg.lineEdit.text()
            # output_file = open(filename, 'w')

            selectedLayerIndex = self.dlg.comboBox.currentIndex()
            selectedLayer = layers[selectedLayerIndex]
            fields = selectedLayer.pendingFields()
            fieldnames = [field.name() for field in fields]

            # PlusCode code
            # Get the field names for the input layer. The will be copied to the output layer with Plus Codes added
            # fields = layer.pendingFields()
            fieldsout = QgsFields(fields)

            # We need to add the plus codes field at the end
            # if fieldsout.append(QgsField(field_name, QVariant.String)) == False:
            #     self.iface.messageBar().pushMessage("", "Plus Codes Field Name must be unique",
            #                                         level=QgsMessageBar.WARNING,duration=4)

            for layer in QgsMapLayerRegistry.instance().mapLayers().values():
                if layer.name() == 'Pins':
                    # pins_layer = layer
                    self.pinLayer = layer
                    self.provider = self.pinLayer.dataProvider()

            layerCRS = layer.crs()
            # pointLayer = QgsVectorLayer("Point?crs={}".format(layerCRS.authid()), layer_name, "memory")
            pointLayer = QgsVectorLayer("Point?crs={}".format(layerCRS.authid()), selectedLayer, "memory")

            # self.pinLayer = QgsVectorLayer(
            #     "Point?crs=epsg:3857&field=id:integer&field=description:string(120)&field=x:double&field=y:double&index=yes",
            #     "Pins",
            #     "memory")

            ppoint = pointLayer.dataProvider()
            ppoint.addAttributes(fieldsout)
            pointLayer.updateFields()

            # The input to the plus codes conversions requires latitudes and longitudes
            # If the layer is not EPSG:4326 we need to convert it.
            if layerCRS != epsg4326:
                transform = QgsCoordinateTransform(layerCRS, epsg4326)

            iter = layer.getFeatures()

            for feature in iter:
                pt = feature.geometry().asPoint()
                if layerCRS != epsg4326:
                    pt = transform.transform(pt)
                try:
                    # msg = olc.encode(pt.y(), pt.x(), precision)
                    msg = olc.encode(pt.y(), pt.x())
                except:
                    msg = ''
                f = QgsFeature()
                f.setGeometry(feature.geometry())
                f.setAttributes(feature.attributes() + [msg])
                ppoint.addFeatures([f])

            pointLayer.updateExtents()
            QgsMapLayerRegistry.instance().addMapLayer(pointLayer)
            self.close()



##################################################################
    # original pluginbuilder code
##################################################################

        # layers = self.iface.legendInterface().layers()
        # layer_list = []
        # for layer in layers:
        #     layer_list.append(layer.name())
        #
        # self.dlg.comboBox.addItems(layer_list)
        #
        # # show the dialog
        # self.dlg.show()
        # # Run the dialog event loop
        # result = self.dlg.exec_()
        # # See if OK was pressed
        # if result:
        #
        #     filename = self.dlg.lineEdit.text()
        #     output_file = open(filename, 'w')
        #
        #     selectedLayerIndex = self.dlg.comboBox.currentIndex()
        #     selectedLayer = layers[selectedLayerIndex]
        #     fields = selectedLayer.pendingFields()
        #     fieldnames = [field.name() for field in fields]
        #
        #     for f in selectedLayer.getFeatures():
        #         line = ','.join(unicode(f[x]) for x in fieldnames) + '\n'
        #         unicode_line = line.encode('utf-8')
        #         output_file.write(unicode_line)
        #     output_file.close()
##################################################################
    # original pluginbuilder code
##################################################################


