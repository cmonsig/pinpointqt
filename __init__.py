# -*- coding: utf-8 -*-
"""
/***************************************************************************
 pinpointQt
                                 A QGIS plugin
 This plugin drops pins on a basemap and displays the pins features when clicked on
                             -------------------
        begin                : 2019-02-17
        copyright            : (C) 2019 by Simone Glinz
        email                : simone@glinz.li
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load pinpointQt class from file pinpointQt.

    :param iface: A QGIS interface instance.
    :type iface: QgisInterface
    """
    #
    from .pinpoint_Qt import pinpointQt
    return pinpointQt(iface)


######################### REMOTE DEBUG #########################
# To activate remote debugging set DEBUG_PLUGIN=RiverscapesToolbar as a QGIS
# Environment variable in Preferences -> System -> Environment
import os
import logging
DEBUG = False


from .debug import InitDebug

if 'DEBUG_PLUGIN_PINPOINTQT' in os.environ and os.environ['DEBUG_PLUGIN_PINPOINTQT'] == "pinpointQt":
    debug.InitDebug()
    DEBUG = True
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

######################### /REMOTE DEBUG #########################